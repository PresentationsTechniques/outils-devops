# Outils Devops

## Collaborate

### LifeCycle

| Produit | Payant    | Remarques           |
|---------|-----------|---------------------|
| Jira    | >10 Users | Référence, Ticketing|
| Trello  | >10 Boards| Kaban Board, Online |
| TFS     | Freemium  | Online              |
| gitlab  | Freemium  | ALM                 |

### Collaboration

| Produit  | Payant   | Remarques          |
|----------|----------|--------------------|
| Slack    |Vidéo 1-1 | Référence, Online  |
| Teams    |Office 365|                    |
|Mattermost|Online    |                    |
|RocketChat|Freemium  |                    |

### Knowledge Management / Doculentation

| Produit       | Payant    | Remarques                     |
|---------------|-----------|-------------------------------|
| Confluence    | >10 Users | Wiki+annotations              |
| MarkDown      | Non       | Norme, Texte enrichit         |
| AsciiDoc      | Non       | Norme, Texte enrichit         |
| Wiki          |           | Référence, Collaboratif       |
| Jekyll        |           | Générateur de pages           |
| Graphviz      | Non       | visualisation de graphes      |
| Plant UML     | Non       | Générateur de diagrammes      |
| API Blueprint |           | Spécification, Orienté exemple|
| RAML          | Non       | texte,Générateur              |
| OpenAPI       |           | Spécification, Norme          |
|Swagger Editor | Freemium  | Editeur basé sur OpenAPI      |
|Swagger CodeGen| Non       | Générateur basé sur OpenAPI   |

## Build

### Source Code Managment

| Produit | Payant    | Remarques |
|---------|-----------|-----------|
| TFS     | Freemium  | Online    |
| GitHub  | Freemium  | Online    |
|bitbucket| >5 Users  | Online    |
| gitlab  | Freemium  |           |

### Continous Integration

| Produit | Payant           | Remarques            |
|---------|------------------|----------------------|
| TFS     | Freemium         | ALM, Online          |
| Jenkins |                  |Ordonnanceur,Référence|
| Bamboo  | >10 Users        |                      |
|Travis CI| Freemium         | Online               |
|Circle CI| Freemium         | Online               |
| CodeShip| >100 builds/month| Online               |
| gitlab  | Freemium         |                      |

### Build

| Produit | Payant    | Remarques       |
|---------|-----------|-----------------|
| Ant     | Non       | Dépassé,Java    |
| Make    | Non       | Complexe        |
| Maven   | Non       | Référence,Java  |
| Gradle  | Non       | Multi langages  |
| Gulp    | Non       | JS              |
| Webpack | Non       | JS              |
| Docker  |           | Complément      |

### Database Management

| Produit   | Payant    | Remarques                     |
|-----------|-----------|-------------------------------|
| DbMaestro | Oui       |                               |
| LiquiBase | Freemium  | SQL, XML, YAML, JSON          |
| Flocker   |           | Orienté Docker                |
| DbDeploy  |           | Java, PHP, .Net               |
| FlyWay    | Freemium  | SQL, Java + Maven/automatique |

## Test

### Testing Unitaire

| Produit   | Payant    | Remarques |
|-----------|-----------|-----------|
| JUnit     | Non       | Java      |
| NUnit     | Non       | .Net      |
| QUnit     | Non       | JS        |
| TestNG    | Non       | Java      |
| Jasmine   | Non       | JS        |
| TestNG    | Non       | Java      |
| TestNG    | Non       | Java      |

### Other Testing

| Produit           | Payant    | Remarques     |
|-------------------|-----------|---------------|
| Fitness           | Non       | Wiki          |
| Karma             | Non       | JS, dur       |
| Jest              | Non       | JS, simple    |
| Cucumber          | Non       | BDD           |
| Robot Framework   | Non       | Automation,BDD|
| Storybook         | Non       | Web, Doc      |
| Selenium          | Non       | Web, IHM      |
| Jmeter            | Non       | Performance   |
| Gatling           | Non       | Performance   |
| Zap               | Non       | Sécurité      |

### Test Management


| Produit           | Payant    | Remarques             |
|-------------------|-----------|-----------------------|
| XRay              | Oui       | Plugin JIRA           |
| Quality Center    | Oui       | HP-ALM                |
| CucumberStudio    | Oui       | Collaboration         |
| Testpad           | Oui       | Drag'n'drop           |
| YEST Team         | Oui       | Conception Graphique  |
| TestLink          | Non       |                       |
| Squash TM         | Freemium  |                       |
| Atom              | Non       | Editeur de texte      |

## Deploy

### Deployement

| Produit       | Payant    | Remarques         |
|---------------|-----------|-------------------|
| SSH           | Non       | protocol          |
| XL deploy     | Payant    | Model-based       |
| Capistrano    | Non       | vieux (2013)      |
| Octopus Deploy| Freemium  | release management|
| Rundeck       | Non       | plugins           |
| Juju          | Non       | LXD               |
| Spinnaker     | Non       | Cloud Native      |

### Continous Deployement

| Produit   | Payant    | Remarques |
|-----------|-----------|-----------|
|Circle CI  | Freemium  | Online    |
| CodeShip  | Freemium  | Online    |
| gitlab    | Freemium  |           |

### Configuration Management

| Produit   | Payant    | Remarques     |
|-----------|-----------|---------------|
| Puppet    | Non       |               |
| Chef      | Non       |               |
| Ansible   | Non       | Référence     |
| Foreman   | Non       | Plugins       |
| Vagrant   | Non       | Orienté VM    |
| TerraForm | Freemium  | Providers     |

### Artefact Management

| Produit           | Payant    | Remarques         |
|-------------------|-----------|-------------------|
| DockerHub         | Freemium  | Private Repos     |
| Docker Registry   | Freemium  | Trusted Registry  |
| Quay              | Oui       | Scann Securité    |
| Artifactory       | Oui       | Universel Securité|
| GitLab            | Freemium  | Docker            |
| Bower             | Non       | Web, Cloud        |
| NPM               | Non       | Web, Cloud        |
| Nuget             | Windows   | .Net              |
| Archiva           | Non       | Historique        |
| Nexus             | Freemium  |Universel,Référence|

## Run

### IaaS / Paas

| Produit   | Payant    | Remarques |
|-----------|-----------|-----------|
| AWS       | Oui       | Référence |
| Heroku    | Freemium  | Cloud     |
| Dokku     | Non       | PaaS      |
| Flynn     | Freemium  | PaaS      |
| Gookle KE | Oui       | IaaS,Cloud|
| MS Azure  | Oui       | IaaS,Cloud|
| OpenStack | Non       | Complexe  |
| OpenShift | Oui       | IaaS      |

### Orchestration

| Produit   | Payant    | Remarques         |
|-----------|-----------|-------------------|
| Mesos     | Non       |                   |
| Mesosphere| Freemium  | Basé sur Mesos    |
| Marathon  | Freemium  | Complexe,Mesos    |
| Swarm     | Non       | Docker            |
| Nomad     | Non       |                   |
| Kubernetes| Non       | Complexe          |
| Rancher 1 | Non       | Simple, hybrid    |
| Rancher 2 | Non       | Kubernetes, hybrid|

### Monitoring

| Produit       | Payant    | Remarques |
|---------------|-----------|-----------|
| Zabbix        | Non       |           |
| Nagios        | Freemium  |           |
| Prometheus    | Non       |           |
| Kibana        | Non       | Graph     |
| Graphana      | Non       | Graph     |
| Splunk        | Freemium  |           |
| Vector        | Non       |           |
| TICK Stack    | Non       | Telegraf, InfluxDB, Chronograf, Kapacitor |

### Logging 

| Produit       | Payant    | Remarques         |
|---------------|-----------|-------------------|
| LogStash      | Freemium  | Centralisation    |
| Grok          | Freemium  | Filtrage          |
| ElasicSearch  | Freemium  | Indexation        |
| Datadog       | Freemium  |                   |
| Zipkin        | Non       | Tracing           |
| Jaeger        | Non       | Tracing           |
| Fluentd       | Non       | Centralisation    |
| Sentry        | Oui       | Error monitoring  |
| New Relic     | Oui       | Cloud             |
| Loki          | Non       | Prometheus        |
| GrayLog       | Freemium  |                   |
